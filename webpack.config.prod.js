/* eslint-disable no-tabs */
/* eslint-disable quotes */
/* eslint-disable sort-keys */
/* eslint-disable key-spacing */
/* eslint-disable comma-dangle */
/* eslint-disable no-undef */
/* eslint-disable quote-props */
const path = require("path");

const CleanPlugin = require("clean-webpack-plugin");

// eslint-disable-next-line no-undef
module.exports = {
	mode: "production",
	// eslint-disable-next-line quote-props
	// eslint-disable-next-line sort-keys
	entry: "./src/app.js",
	// eslint-disable-next-line quote-props
	output: {
		filename: "[contenthash].js",
		path: path.resolve(__dirname, "assets", "scripts"),
		publicPath: "assets/scripts/",
	},
	devtool: "cheap-source-map",
	plugins: [new CleanPlugin.CleanWebpackPlugin()],
};
